require "bundler/setup"
require "junit_xml_experiments"

RSpec.configure do |config|
  # Enable flags like --only-failures and --next-failure
  config.example_status_persistence_file_path = ".rspec_status"

  # Disable RSpec exposing methods globally on `Module` and `main`
  config.disable_monkey_patching!

  config.expect_with :rspec do |c|
    c.syntax = :expect
  end

  # register around filter that captures stdout and stderr
  # config.around(:each) do |example|
  #   $stdout = StringIO.new
  #   $stderr = StringIO.new

  #   example.run

  #   example.metadata[:stdout] = $stdout.string
  #   example.metadata[:stderr] = $stderr.string

  #   $stdout = STDOUT
  #   $stderr = STDERR
  # end
end
